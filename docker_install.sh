sudo apt update;
sudo apt upgrade -y;
sudo apt autoremove -y;
sudo apt install -y apt-transport-https ca-certificates curl gnupg software-properties-common;
sudo apt-get remove docker docker-engine docker.io containerd runc;
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - ;
sudo apt-key fingerprint 0EBFCD88;
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt-get update;
apt-cache policy docker-ce;
sudo apt-get install -y docker-ce docker-ce-cli containerd.io;
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose;
sudo chmod +x /usr/local/bin/docker-compose;
sudo apt-get install -y build-essential;
sudo apt-get install -y libssl-dev ;
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash ;
echo "Close the Terminal";
