sudo apt install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common;
sudo apt update;
sudo apt upgrade -y;
sudo apt autoremove -y;
curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | sudo apt-key add - ;
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable";
sudo apt update;
sudo mkdir -p /rw/config/qubes-bind-dirs.d;
sudo cat << EOF > /rw/config/qubes-bind-dirs.d/50_user.conf
binds+=( '/var/lib/docker' )
binds+=( '/etc/docker' )
EOF ;
sudo apt install -y docker-ce;
sudo groupadd docker;
sudo usermod -aG docker user;
sudo systemctl enable docker;
sudo apt install -y docker-ce-cli containerd.io;
sudo curl -L https://github.com/docker/compose/releases/download/1.26.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose;
sudo chmod +x /usr/local/bin/docker-compose;
